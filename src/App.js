import 'bootstrap/dist/css/bootstrap.min.css'
import ExampleCallFetch from './components/exampleCallFetch';

function App() {
  return (
    <div>
      <ExampleCallFetch/>
    </div>
  );
}

export default App;
