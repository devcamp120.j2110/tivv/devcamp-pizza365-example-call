import { Component } from "react";

class ExampleCallFetch extends Component {
    fetchApi = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl, paramOption);
        const responseData = await response.json();
        return responseData;
    }
    APIGetAll = () => {
        this.fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders')
            .then((data) => {
                console.log(data)
            })
            .catch(console.error());
    }
    APICreate = () => {
        this.fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders', {
            method: 'POST',
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
            .then((data) => {
                console.log(data)
            })
            .catch(console.error());

    }
    APIGetById = () => {
        this.fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders/yFj9cRkOQN')
            .then((data) => {
                console.log(data)
            })
            .catch(console.error());
    }
    APIUpdate = () => {
        this.fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders/20088', {
            method: 'PUT',
            body: JSON.stringify({
                trangThai: "confirmed"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
            .then((data) => {
                console.log(data)
            })
            .catch(console.error());

    }
    APICheckById = () => {
        this.fetchApi('http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/12332')
            .then((data) => {
                console.log(data)
            })
            .catch(console.error());
    }
    APIGetDrink = () => {
        this.fetchApi('http://42.115.221.44:8080/devcamp-pizza365/drinks')
            .then((data) => {
                console.log(data)
            })
            .catch(console.error());
    }
    render() {
        return (
            <div className="container pt-4 bg-light">
                Test Page for Javascript Tasks. F5 to run code.
                <div className="row">
                    <div className="col-2">
                        <button className="btn btn-primary" onClick={this.APIGetAll}>Call API Get All Order</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-info" onClick={this.APICreate}>Call API Create Order</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-success" onClick={this.APIGetById}>Call API Get Order By Id</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-warning" onClick={this.APIUpdate}>Call API Update Order</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-secondary" onClick={this.APICheckById}>Call API Check VC By Id</button>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-danger" onClick={this.APIGetDrink}>Call API Get Drink List</button>
                    </div>
                </div>
                <div className="row">
                    <p>
                        Demo 06 API for Pizza 365 Project:
                    </p>
                    <ul>
                        <li>Get all orders: lấy tất cả order</li>
                        <li>Create order: tạo 1 order</li>
                        <li>get order by id: lấy 01 order bằng Id</li>
                        <li>Update Order: update 01 order</li>
                        <li>Check voucher by Id: Check thông tin mã giảm giá, quan trọng là có hay không và phần trăm giảm giá</li>
                        <li>Get drinks list: lấy danh sách đồ uống</li>
                    </ul>
                    <p style={{color:"red"}}>Bật console để nhìn rõ output</p>
                </div>
            </div>
        )
    }
}
export default ExampleCallFetch;